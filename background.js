const API_URL = " http://83.136.233.161/api/meme"
const browser_object = chrome

async function imgSaveListener(info, tab) {
    let resp
    switch (info.menuItemId) {
        case "img-save":
            resp = await saveImage(info.srcUrl)
            console.log(resp)
            break
        case "url-save":
            resp = await saveImage(info.linkUrl)
            console.log(resp)
            break
    }
}

async function saveImage(source_url) {
    const data = {
        body: JSON.stringify({
            url: source_url
        }),
        method: "POST",
        headers: {
            "content-type": "application/json; charset=UTF-8"
        }
    }
    let resp = await fetch(API_URL, data)
    let resp_json = await resp.json()
    return JSON.stringify(resp_json)

}

browser_object.contextMenus.create({
    id: "img-save",
    title: "Shitpost this image",
    contexts: ["image"]
})
browser_object.contextMenus.create({
    id: "url-save",
    title: "Shitpost this image",
    contexts: ["link"]
})

browser_object.contextMenus.onClicked.addListener(imgSaveListener)